#!/bin/bash

function printhelp() {
  cat <<-EOF
Usage: smv [OPTION] [[user@]host1:]file1 [[user@]host2:]file2
smv is a scp-like to move files instead of copy them.

  -h, --help    Display this help and exit
  -r            Recursively copy entire directories.  Note that smv follows symbolic links encountered in the tree traversal.

NB: To avoid to type the password multiple times, I would recommend tu use ssh keys.

Last versions: <https://framagit.org/SecT0uch/Tools/tree/master/smv>
EOF
exit 0
}

if (( "$#" < 2 )); then
  printhelp
fi

for i in "$@"; do
  if [ `echo $i` ==  '-h' ] || [ `echo $i` == '--help' ]; then
    printhelp
  fi
done

src="${*%${!#}}"   # args exept the last
dest="${!#}"       # last arg.

scp $src $dest
if [ $? -eq 0 ]; then          # Check if scp ended with no error
  if [[ "$src" == *':'* ]]; then
    srchost=`echo $src | cut -f 1 -d ":" | sed 's/-r//'`
    srcpath=`echo $src | cut -f 2 -d ":"`
    ssh $srchost rm -r $srcpath
  else
    rm -r $src
  fi
fi
