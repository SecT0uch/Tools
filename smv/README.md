# smv
**smv** is a **scp-like** to move files instead of copy them.

### Usage
```
smv [OPTION] [[user@]host1:]file1 [[user@]host2:]file2

  -h, --help    Display this help and exit
  -r            Recursively copy entire directories.  Note that smv follows symbolic links encountered in the tree traversal.

NB: To avoid to type the password multiple times, I would recommend tu use ssh keys.
```
