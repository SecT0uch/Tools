#!/bin/bash

request=`curl -i -H "Content-type: application/json" -X POST http://mafreebox.freebox.fr/api/v4/login/authorize/ -d '
{
   "app_id": "Kodi.app",
   "app_name": "Kodi Control",
   "app_version": "0.1",
   "device_name": "MediaCenter"
}    ' 2>/dev/null`

app_token=`echo $request | cut -f 8 -d '"'`
track_id=`echo $request | cut -f 12 -d ':'  | sed 's/\}//g'`

printf "Please authorize your new app through the screen of your Freebox.\n Waiting for physical authorizathon...\n"


status="pending"

while [ "$status" ==  "pending" ]; do
    result=`curl -i -H "Content-type: application/json" -X GET http://mafreebox.freebox.fr/api/v4/login/authorize/$track_id 2>/dev/null`
    status=`echo $result | cut -f 8 -d '"'`
    challenge=`echo $result | cut -f 12 -d '"'`
done

if [ "$status" == "unknown" ]; then
    printf "The app_token is invalid or has been revoked\n"
fi

if [ "$status" == "timeout" ]; then
    printf "The user did not confirmed the authorization within the given time\n"
fi

if [ "$status" == "denied" ]; then
    printf "The user denied the authorization request\n"
fi

if [ "$status" == "granted" ]; then
    printf "The app_token is now valid and can be used to open a session\n"
    printf "app_token : $app_token\n"
    echo -n $app_token > app_token
fi
