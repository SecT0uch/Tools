# FreeboxTokenGen.sh

This script let you register an application to control your **Freebox Server (v6)**.
It generates the app_token file.

## Compatibility

This script works with the **FreeboxOS 3.4** (last to this day), using the **API version 4**.

## Usage

Edit the lines:
* "app_id":
* "app_name":
* "app_version":
* "device_name":

The scripts writes the file **app_token** which you will need to authenticate your app.

You can now choose which permissions your application will get going [here] (http://mafreebox.free.fr/#Fbx.os.app.settings.Accounts), tab "Applications"

Your next step is to [obtain a session-token](https://dev.freebox.fr/sdk/os/login/).
You can see an example [in one of my scripts](https://framagit.org/SecT0uch/Kodi/blob/master/mvAnimes/mvAnimes.sh)

## Documentation

The Developper API Documentation is available here :
https://dev.freebox.fr/sdk/os/
